﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class jeu2j
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(jeu2j))
        Me.cmdMenu = New System.Windows.Forms.Button()
        Me.quitter = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cmdEchanger = New System.Windows.Forms.Button()
        Me.cmdSST = New System.Windows.Forms.Button()
        Me.cmdAbandonner = New System.Windows.Forms.Button()
        Me.cmdManche = New System.Windows.Forms.Button()
        Me.j1 = New System.Windows.Forms.Label()
        Me.j1points = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.j2 = New System.Windows.Forms.Label()
        Me.j2points = New System.Windows.Forms.Label()
        Me.nbpieces = New System.Windows.Forms.Label()
        Me.cmdReturner = New System.Windows.Forms.Button()
        Me.grpPions = New System.Windows.Forms.GroupBox()
        Me.pic1 = New System.Windows.Forms.PictureBox()
        Me.pic2 = New System.Windows.Forms.PictureBox()
        Me.pic6 = New System.Windows.Forms.PictureBox()
        Me.pic3 = New System.Windows.Forms.PictureBox()
        Me.pic5 = New System.Windows.Forms.PictureBox()
        Me.pic4 = New System.Windows.Forms.PictureBox()
        Me.cmdG = New System.Windows.Forms.Button()
        Me.cmdP = New System.Windows.Forms.Button()
        Me.grpPions.SuspendLayout()
        CType(Me.pic1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pic2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pic6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pic3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pic5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pic4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'cmdMenu
        '
        Me.cmdMenu.BackColor = System.Drawing.Color.DodgerBlue
        Me.cmdMenu.Font = New System.Drawing.Font("Comic Sans MS", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdMenu.ForeColor = System.Drawing.SystemColors.HighlightText
        Me.cmdMenu.Location = New System.Drawing.Point(1, 6)
        Me.cmdMenu.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.cmdMenu.Name = "cmdMenu"
        Me.cmdMenu.Size = New System.Drawing.Size(103, 41)
        Me.cmdMenu.TabIndex = 15
        Me.cmdMenu.Text = "Menu"
        Me.cmdMenu.UseVisualStyleBackColor = False
        '
        'quitter
        '
        Me.quitter.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.quitter.Font = New System.Drawing.Font("Franklin Gothic Medium", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.quitter.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.quitter.Location = New System.Drawing.Point(1004, 0)
        Me.quitter.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.quitter.Name = "quitter"
        Me.quitter.Size = New System.Drawing.Size(60, 55)
        Me.quitter.TabIndex = 14
        Me.quitter.Text = "X"
        Me.quitter.UseVisualStyleBackColor = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Comic Sans MS", 48.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.LightSkyBlue
        Me.Label1.Location = New System.Drawing.Point(331, -10)
        Me.Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(342, 111)
        Me.Label1.TabIndex = 13
        Me.Label1.Text = "Qwirkle"
        '
        'cmdEchanger
        '
        Me.cmdEchanger.BackColor = System.Drawing.Color.Orchid
        Me.cmdEchanger.Font = New System.Drawing.Font("Comic Sans MS", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdEchanger.ForeColor = System.Drawing.SystemColors.HighlightText
        Me.cmdEchanger.Location = New System.Drawing.Point(876, 265)
        Me.cmdEchanger.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.cmdEchanger.Name = "cmdEchanger"
        Me.cmdEchanger.Size = New System.Drawing.Size(188, 41)
        Me.cmdEchanger.TabIndex = 16
        Me.cmdEchanger.Text = "Echanger"
        Me.cmdEchanger.UseVisualStyleBackColor = False
        '
        'cmdSST
        '
        Me.cmdSST.BackColor = System.Drawing.Color.DarkGoldenrod
        Me.cmdSST.Font = New System.Drawing.Font("Comic Sans MS", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdSST.ForeColor = System.Drawing.SystemColors.HighlightText
        Me.cmdSST.Location = New System.Drawing.Point(876, 326)
        Me.cmdSST.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.cmdSST.Name = "cmdSST"
        Me.cmdSST.Size = New System.Drawing.Size(188, 41)
        Me.cmdSST.TabIndex = 17
        Me.cmdSST.Text = "Sauter son tour"
        Me.cmdSST.UseVisualStyleBackColor = False
        '
        'cmdAbandonner
        '
        Me.cmdAbandonner.BackColor = System.Drawing.Color.Firebrick
        Me.cmdAbandonner.Font = New System.Drawing.Font("Comic Sans MS", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdAbandonner.ForeColor = System.Drawing.SystemColors.HighlightText
        Me.cmdAbandonner.Location = New System.Drawing.Point(112, 6)
        Me.cmdAbandonner.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.cmdAbandonner.Name = "cmdAbandonner"
        Me.cmdAbandonner.Size = New System.Drawing.Size(147, 41)
        Me.cmdAbandonner.TabIndex = 18
        Me.cmdAbandonner.Text = "Abandonner"
        Me.cmdAbandonner.UseVisualStyleBackColor = False
        '
        'cmdManche
        '
        Me.cmdManche.BackColor = System.Drawing.Color.LimeGreen
        Me.cmdManche.Location = New System.Drawing.Point(876, 210)
        Me.cmdManche.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.cmdManche.Name = "cmdManche"
        Me.cmdManche.Size = New System.Drawing.Size(92, 33)
        Me.cmdManche.TabIndex = 19
        Me.cmdManche.Text = "✔"
        Me.cmdManche.UseVisualStyleBackColor = False
        '
        'j1
        '
        Me.j1.AutoSize = True
        Me.j1.Font = New System.Drawing.Font("Comic Sans MS", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.j1.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.j1.Location = New System.Drawing.Point(233, 101)
        Me.j1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.j1.Name = "j1"
        Me.j1.Size = New System.Drawing.Size(138, 35)
        Me.j1.TabIndex = 22
        Me.j1.Text = "Joueur 1 :"
        '
        'j1points
        '
        Me.j1points.AutoSize = True
        Me.j1points.Font = New System.Drawing.Font("Comic Sans MS", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.j1points.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.j1points.Location = New System.Drawing.Point(389, 101)
        Me.j1points.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.j1points.Name = "j1points"
        Me.j1points.Size = New System.Drawing.Size(30, 35)
        Me.j1points.TabIndex = 23
        Me.j1points.Text = "0"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Comic Sans MS", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Label5.Location = New System.Drawing.Point(921, 476)
        Me.Label5.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(85, 35)
        Me.Label5.TabIndex = 24
        Me.Label5.Text = "pièces"
        '
        'j2
        '
        Me.j2.AutoSize = True
        Me.j2.Font = New System.Drawing.Font("Comic Sans MS", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.j2.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.j2.Location = New System.Drawing.Point(603, 101)
        Me.j2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.j2.Name = "j2"
        Me.j2.Size = New System.Drawing.Size(138, 35)
        Me.j2.TabIndex = 24
        Me.j2.Text = "Joueur 2 :"
        '
        'j2points
        '
        Me.j2points.AutoSize = True
        Me.j2points.Font = New System.Drawing.Font("Comic Sans MS", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.j2points.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.j2points.Location = New System.Drawing.Point(759, 101)
        Me.j2points.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.j2points.Name = "j2points"
        Me.j2points.Size = New System.Drawing.Size(30, 35)
        Me.j2points.TabIndex = 25
        Me.j2points.Text = "0"
        '
        'nbpieces
        '
        Me.nbpieces.AutoSize = True
        Me.nbpieces.Font = New System.Drawing.Font("Comic Sans MS", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nbpieces.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.nbpieces.Location = New System.Drawing.Point(881, 476)
        Me.nbpieces.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.nbpieces.Name = "nbpieces"
        Me.nbpieces.Size = New System.Drawing.Size(45, 35)
        Me.nbpieces.TabIndex = 25
        Me.nbpieces.Text = "96"
        '
        'cmdReturner
        '
        Me.cmdReturner.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.cmdReturner.Location = New System.Drawing.Point(965, 212)
        Me.cmdReturner.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.cmdReturner.Name = "cmdReturner"
        Me.cmdReturner.Size = New System.Drawing.Size(91, 32)
        Me.cmdReturner.TabIndex = 26
        Me.cmdReturner.Text = "<----"
        Me.cmdReturner.UseVisualStyleBackColor = False
        '
        'grpPions
        '
        Me.grpPions.AutoSize = True
        Me.grpPions.BackColor = System.Drawing.Color.Transparent
        Me.grpPions.Controls.Add(Me.pic1)
        Me.grpPions.Controls.Add(Me.pic2)
        Me.grpPions.Controls.Add(Me.pic6)
        Me.grpPions.Controls.Add(Me.pic3)
        Me.grpPions.Controls.Add(Me.pic5)
        Me.grpPions.Controls.Add(Me.pic4)
        Me.grpPions.ForeColor = System.Drawing.Color.Transparent
        Me.grpPions.Location = New System.Drawing.Point(143, 580)
        Me.grpPions.Margin = New System.Windows.Forms.Padding(0)
        Me.grpPions.Name = "grpPions"
        Me.grpPions.Padding = New System.Windows.Forms.Padding(0)
        Me.grpPions.Size = New System.Drawing.Size(313, 80)
        Me.grpPions.TabIndex = 33
        Me.grpPions.TabStop = False
        '
        'pic1
        '
        Me.pic1.Image = Global.Qwirkle.My.Resources.Resources._1_1
        Me.pic1.Location = New System.Drawing.Point(27, 20)
        Me.pic1.Margin = New System.Windows.Forms.Padding(0)
        Me.pic1.Name = "pic1"
        Me.pic1.Size = New System.Drawing.Size(39, 42)
        Me.pic1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.pic1.TabIndex = 27
        Me.pic1.TabStop = False
        '
        'pic2
        '
        Me.pic2.Image = Global.Qwirkle.My.Resources.Resources._4_2
        Me.pic2.Location = New System.Drawing.Point(72, 20)
        Me.pic2.Margin = New System.Windows.Forms.Padding(0)
        Me.pic2.Name = "pic2"
        Me.pic2.Size = New System.Drawing.Size(39, 42)
        Me.pic2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.pic2.TabIndex = 32
        Me.pic2.TabStop = False
        '
        'pic6
        '
        Me.pic6.Image = Global.Qwirkle.My.Resources.Resources._5_1
        Me.pic6.Location = New System.Drawing.Point(253, 20)
        Me.pic6.Margin = New System.Windows.Forms.Padding(0)
        Me.pic6.Name = "pic6"
        Me.pic6.Size = New System.Drawing.Size(39, 42)
        Me.pic6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.pic6.TabIndex = 28
        Me.pic6.TabStop = False
        '
        'pic3
        '
        Me.pic3.Image = Global.Qwirkle.My.Resources.Resources._6_1
        Me.pic3.Location = New System.Drawing.Point(117, 20)
        Me.pic3.Margin = New System.Windows.Forms.Padding(0)
        Me.pic3.Name = "pic3"
        Me.pic3.Size = New System.Drawing.Size(39, 42)
        Me.pic3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.pic3.TabIndex = 31
        Me.pic3.TabStop = False
        '
        'pic5
        '
        Me.pic5.Image = Global.Qwirkle.My.Resources.Resources._3_4
        Me.pic5.Location = New System.Drawing.Point(208, 20)
        Me.pic5.Margin = New System.Windows.Forms.Padding(0)
        Me.pic5.Name = "pic5"
        Me.pic5.Size = New System.Drawing.Size(39, 42)
        Me.pic5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.pic5.TabIndex = 29
        Me.pic5.TabStop = False
        '
        'pic4
        '
        Me.pic4.Image = Global.Qwirkle.My.Resources.Resources._2_6
        Me.pic4.Location = New System.Drawing.Point(163, 20)
        Me.pic4.Margin = New System.Windows.Forms.Padding(0)
        Me.pic4.Name = "pic4"
        Me.pic4.Size = New System.Drawing.Size(39, 42)
        Me.pic4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.pic4.TabIndex = 30
        Me.pic4.TabStop = False
        '
        'cmdG
        '
        Me.cmdG.BackgroundImage = CType(resources.GetObject("cmdG.BackgroundImage"), System.Drawing.Image)
        Me.cmdG.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.cmdG.Location = New System.Drawing.Point(16, 162)
        Me.cmdG.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.cmdG.Name = "cmdG"
        Me.cmdG.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdG.Size = New System.Drawing.Size(51, 47)
        Me.cmdG.TabIndex = 21
        Me.cmdG.UseVisualStyleBackColor = True
        '
        'cmdP
        '
        Me.cmdP.BackgroundImage = CType(resources.GetObject("cmdP.BackgroundImage"), System.Drawing.Image)
        Me.cmdP.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.cmdP.Location = New System.Drawing.Point(16, 228)
        Me.cmdP.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.cmdP.Name = "cmdP"
        Me.cmdP.Size = New System.Drawing.Size(51, 47)
        Me.cmdP.TabIndex = 20
        Me.cmdP.UseVisualStyleBackColor = True
        '
        'jeu2j
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(1072, 630)
        Me.Controls.Add(Me.cmdReturner)
        Me.Controls.Add(Me.j2points)
        Me.Controls.Add(Me.nbpieces)
        Me.Controls.Add(Me.j2)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.j1points)
        Me.Controls.Add(Me.j1)
        Me.Controls.Add(Me.cmdG)
        Me.Controls.Add(Me.cmdP)
        Me.Controls.Add(Me.cmdManche)
        Me.Controls.Add(Me.cmdAbandonner)
        Me.Controls.Add(Me.cmdSST)
        Me.Controls.Add(Me.cmdEchanger)
        Me.Controls.Add(Me.cmdMenu)
        Me.Controls.Add(Me.quitter)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.grpPions)
        Me.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Name = "jeu2j"
        Me.Text = "Form1"
        Me.grpPions.ResumeLayout(False)
        CType(Me.pic1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pic2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pic6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pic3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pic5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pic4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents cmdMenu As Button
    Friend WithEvents quitter As Button
    Friend WithEvents Label1 As Label
    Friend WithEvents cmdEchanger As Button
    Friend WithEvents cmdSST As Button
    Friend WithEvents cmdAbandonner As Button
    Friend WithEvents cmdManche As Button
    Friend WithEvents cmdP As Button
    Friend WithEvents cmdG As Button
    Friend WithEvents j1 As Label
    Friend WithEvents j1points As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents j2 As Label
    Friend WithEvents j2points As Label
    Friend WithEvents nbpieces As Label
    Friend WithEvents cmdReturner As Button
    Friend WithEvents pic1 As PictureBox
    Friend WithEvents pic6 As PictureBox
    Friend WithEvents pic5 As PictureBox
    Friend WithEvents pic4 As PictureBox
    Friend WithEvents pic3 As PictureBox
    Friend WithEvents pic2 As PictureBox
    Friend WithEvents grpPions As GroupBox
End Class
