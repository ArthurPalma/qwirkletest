﻿
Imports QwirkleLib
Public Class frPagemenu

    Private Sub jouer_Click(sender As Object, e As EventArgs) Handles jouer.Click
        Me.Hide()
        nbjoueurs.Show()
    End Sub

    Private Sub scores_Click(sender As Object, e As EventArgs) Handles scores.Click
        Me.Hide()
        score.Show()
    End Sub

    Private Sub reglesdujeu_Click(sender As Object, e As EventArgs) Handles reglesdujeu.Click
        Me.Hide()
        regles.Show()
    End Sub

    Private Sub quitter_Click(sender As Object, e As EventArgs) Handles quitter.Click
        Dim Rep As DialogResult
        Rep = MessageBox.Show("Voulez-vous vraiment quitter le jeu?", "Message", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
        If Rep = DialogResult.Yes Then
            Me.Close()
        End If
    End Sub

End Class
