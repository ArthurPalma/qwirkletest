﻿Public Class jeu4j

    '现在没有解决的问题：
    '1.放下棋子之后 如果想用别的棋子替换 怎么换
    '2.点了勾键 但是除了中心位别的还是不能下
    '3.判断棋子周围没有下棋子的地方
    '4.能下的棋子 限制条件（颜色相同或者 图案相同
    '5.随机
    '6.离边框还有一格的时候 new 出新的边框



    '轮数
    Dim nbManche As Integer = 0

    '边框
    Dim griW As Integer = 0
    Dim griH As Integer = 0

    '已填图案的最大边框
    Dim maxl As Integer = 0
    Dim maxt As Integer = 0
    Dim maxr As Integer = 0
    Dim maxb As Integer = 0

    'pic的大小
    Dim w As Integer = 34

    '自定义事件 落下棋子的时候触发 开放周围的pic可以放棋子




    'pions文件夹所在路径



    Private Sub jeu_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim i As Byte = 0
        Dim u As Byte = 0
        Dim x As Integer = 84
        Dim y As Integer = 132


        '初始化棋盘


        Dim pic As PictureBox
        Dim taille As New System.Drawing.Size
        taille.Height = w
        taille.Width = w

        While i < 13 And u < 9
            While i < 13
                pic = New PictureBox
                Dim lieu As New Point
                lieu.X = x
                lieu.Y = y

                pic.Name = "pic" & i.ToString & "_" & u.ToString
                pic.Size = taille
                pic.Location = lieu
                pic.Visible = True
                pic.BackColor = Color.Gray
                pic.BorderStyle = BorderStyle.FixedSingle
                pic.SizeMode = PictureBoxSizeMode.StretchImage


                Me.Controls.Add(pic)
                x += w

                If x = w * 13 + 84 Then
                    x = 84
                    y += w
                End If
                i += 1
            End While
            u += 1
            i = 0
        End While

        griH = 8
        griW = 12

        '初始化pions


        '通过轮数 判断什么地方能allowdrop
        '第一轮只有中间能下
        Dim Rien As Boolean = True
        For i = 0 To griW
            For u = 0 To griH
                pic = Controls("pic" & i.ToString & "_" & u.ToString)
                If pic.Image IsNot Nothing Then
                    Rien = False
                End If
            Next
        Next

        If Rien = True Then
            Dim picM As PictureBox
            picM = Controls("pic" & 6.ToString & "_" & 4.ToString)
            picM.BorderStyle = BorderStyle.Fixed3D
            picM.AllowDrop = True
            AddHandler picM.DragEnter, AddressOf picM_DragEnter
            AddHandler picM.DragDrop, AddressOf picM_DragDrop

        Else
            For i = 0 To griW
                For u = 0 To griH
                    Dim picM As PictureBox
                    Dim pic1 As PictureBox
                    Dim pic2 As PictureBox
                    Dim pic3 As PictureBox
                    Dim pic4 As PictureBox
                    picM = Controls("pic" & i.ToString & "_" & u.ToString)
                    pic1 = Controls("pic" & i + 1.ToString & "_" & u.ToString)
                    pic2 = Controls("pic" & i - 1.ToString & "_" & u.ToString)
                    pic3 = Controls("pic" & i.ToString & "_" & u + 1.ToString)
                    pic4 = Controls("pic" & i.ToString & "_" & u - 1.ToString)

                    If (pic1.Image Is Nothing AndAlso pic2.Image Is Nothing AndAlso pic3.Image Is Nothing AndAlso pic4.Image Is Nothing Or picM IsNot Nothing) Then
                        picM.AllowDrop = False
                    Else
                        picM.AllowDrop = True
                        picM.BorderStyle = BorderStyle.Fixed3D
                    End If
                    AddHandler picM.DragEnter, AddressOf picM_DragEnter
                    AddHandler picM.DragDrop, AddressOf picM_DragDrop
                Next
            Next

        End If


        For i = 2 To 6
            AddHandler grpPions.Controls("pic" & i.ToString).MouseMove, AddressOf picPions_MouseMove
        Next

        '右移
        If (griW - maxr - maxr) = 2 Then
            i = maxl + 1
            u = maxt + 1
            x = 84
            y = 132

            Dim picR As PictureBox

            For i = 0 To maxl
                For u = 0 To maxt
                    'If picR Then
                Next
            Next
        End If

        '左移
        If (griW - maxr - maxr) = -2 Then

        End If

        '下移
        If (griW - maxb - maxt) = 2 Then

        End If

        '上移
        If (griW - maxb - maxt) = -2 Then

        End If

        '下加一行
        i = 0
        u = 0
        x = 84
        y = 132
        If (griH - maxb) = 0 Then
            While i < griW
                pic = New PictureBox
                Dim lieu As New Point
                lieu.X = x
                lieu.Y = y + griH * w

                pic.Name = "pic" & i.ToString & griH.ToString
                pic.Size = taille
                pic.Location = lieu
                pic.Visible = True
                pic.BackColor = Color.Gray
                pic.BorderStyle = BorderStyle.FixedSingle
                pic.SizeMode = PictureBoxSizeMode.StretchImage

                Me.Controls.Add(pic)
                x += w
                i += 1
            End While
        End If

        '右加一行
        i = 0
        u = 0
        x = 84
        y = 132
        If (griW - maxr) = 0 Then
            While u < griH
                pic = New PictureBox
                Dim lieu As New Point
                lieu.X = x + griW * w
                lieu.Y = y

                pic.Name = "pic" & griW.ToString & u.ToString
                pic.Size = taille
                pic.Location = lieu
                pic.Visible = True
                pic.BackColor = Color.Gray
                pic.BorderStyle = BorderStyle.FixedSingle
                pic.SizeMode = PictureBoxSizeMode.StretchImage

                Me.Controls.Add(pic)
                y += w
                u += 1
            End While
        End If

    End Sub

    Private Sub picPions_MouseMove(sender As Object, e As MouseEventArgs)
        Dim pic As PictureBox = sender
        Dim rep As DragDropEffects

        If e.Button = MouseButtons.Left Then
            rep = pic.DoDragDrop(pic.Image, DragDropEffects.Move)
            If rep = DragDropEffects.Move Then
                pic.Image = Nothing
            End If
        End If

    End Sub

    Private Sub picM_DragEnter(sender As Object, e As DragEventArgs)
        If e.Data.GetDataPresent(DataFormats.Bitmap) Then
            e.Effect = DragDropEffects.Move
        Else
            e.Effect = DragDropEffects.None
        End If
    End Sub

    Private Sub picM_DragDrop(sender As Object, e As DragEventArgs)
        Dim pic As PictureBox = sender
        Dim iu As String
        Dim tabiu As String()
        Dim i As Integer
        Dim u As Integer
        Dim picVoisins As List(Of PictureBox)

        pic.Image = e.Data.GetData(DataFormats.Bitmap)
        pic.AllowDrop = False

        'vérification pose possible


        'activation des voisins
        iu = pic.Name.Substring(3)
        tabiu = iu.Split(CChar("_"))
        i = tabiu(0)
        u = tabiu(1)

        picVoisins = New List(Of PictureBox)
        picVoisins.Add(Controls("pic" & i + 1.ToString & "_" & u.ToString))
        picVoisins.Add(Controls("pic" & i - 1.ToString & "_" & u.ToString))
        picVoisins.Add(Controls("pic" & i.ToString & "_" & u + 1.ToString))
        picVoisins.Add(Controls("pic" & i.ToString & "_" & u - 1.ToString))


        For Each V As PictureBox In picVoisins
            If V.Image Is Nothing Then
                V.AllowDrop = True
                V.BorderStyle = BorderStyle.Fixed3D
                AddHandler V.DragEnter, AddressOf picM_DragEnter
                AddHandler V.DragDrop, AddressOf picM_DragDrop
            End If
        Next



    End Sub
    '摁下勾键 轮数加一
    Private Sub cmdManche_Click(sender As Object, e As EventArgs) Handles cmdManche.Click
        nbManche += 1

        Dim picV As PictureBox
        For i = 0 To griW
            For u = 0 To griH
                picV = Controls("pic" & i.ToString & u.ToString)
                If picV.Image IsNot Nothing Then
                    picV.AllowDrop = False
                End If
            Next
        Next
    End Sub

    '变大
    Private Sub CmdG_Click(sender As Object, e As EventArgs) Handles cmdG.Click
        Dim i As Byte = 0
        Dim u As Byte = 0
        Dim x As Integer = 84
        Dim y As Integer = 132

        w += 1

        Dim pic As PictureBox

        Dim taille As New System.Drawing.Size
        taille.Height = w
        taille.Width = w

        While i < (griW + 1) And u < (griH + 1)
            While i < (griW + 1)
                Dim lieu As New Point
                lieu.X = x
                lieu.Y = y
                pic = Controls("pic" & i.ToString & u.ToString)
                pic.Size = taille
                pic.Location = lieu

                Me.Controls.Add(pic)
                x += w

                If x = w * (griW + 1) + 84 Then
                    x = 84
                    y += w
                End If
                i += 1
            End While
            u += 1
            i = 0
        End While
    End Sub

    '变小
    Private Sub CmdP_Click(sender As Object, e As EventArgs) Handles cmdP.Click
        Dim i As Byte = 0
        Dim u As Byte = 0
        Dim x As Integer = 84
        Dim y As Integer = 132

        w -= 1

        Dim pic As PictureBox

        Dim taille As New System.Drawing.Size
        taille.Height = w
        taille.Width = w

        While i < (griW + 1) And u < (griH + 1)
            While i < (griW + 1)
                Dim lieu As New Point
                lieu.X = x
                lieu.Y = y
                pic = Controls("pic" & i.ToString & u.ToString)
                pic.Size = taille
                pic.Location = lieu

                Me.Controls.Add(pic)
                x += w

                If x = w * (griW + 1) + 84 Then
                    x = 84
                    y += w
                End If
                i += 1
            End While
            u += 1
            i = 0
        End While
    End Sub

    Private Sub CmdReturner_Click(sender As Object, e As EventArgs) Handles cmdReturner.Click

    End Sub

    Private Sub CmdSST_Click(sender As Object, e As EventArgs) Handles cmdSST.Click
        nbManche += 1
    End Sub

    Private Sub quitter_Click(sender As Object, e As EventArgs) Handles quitter.Click
        Dim Rep As DialogResult
        Rep = MessageBox.Show("Voulez-vous vraiment quitter le jeu?", "Message", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
        If Rep = DialogResult.Yes Then
            Me.Close()
        End If
    End Sub

    Private Sub cmdMenu_Click(sender As Object, e As EventArgs) Handles cmdMenu.Click
        Dim Rep1 As DialogResult
        Rep1 = MessageBox.Show("En réalisant cette opération, vous abandonnez la partie. Voulez-vous vraiment le faire?", "Message", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
        If Rep1 = DialogResult.Yes Then
            Me.Hide()
            frPagemenu.Show()
        End If
    End Sub

    Private Sub cmdAbandonner_Click(sender As Object, e As EventArgs) Handles cmdAbandonner.Click
        Dim Rep2 As DialogResult
        Rep2 = MessageBox.Show("Voulez-vous vraiment abandonner la partie?", "Message", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
        If Rep2 = DialogResult.Yes Then
            Me.Hide()
            frPagemenu.Show()
        End If
    End Sub

    Private Sub cmdEchanger_DrapDrop(sender As Object, e As EventArgs) Handles cmdEchanger.DragDrop
        ' MAJhand()
    End Sub

    Private Sub j1_TextChanged(sender As Object, e As EventArgs) Handles j1.TextChanged
        j1.Text = formulaire4j.infoj1.Text + " :"
    End Sub

    Private Sub j2_TextChanged(sender As Object, e As EventArgs) Handles j2.TextChanged
        j2.Text = formulaire4j.infoj2.Text + " :"
    End Sub

    Private Sub j3_TextChanged(sender As Object, e As EventArgs) Handles j3.TextChanged
        j3.Text = formulaire4j.infoj3.Text + " :"
    End Sub

    Private Sub j4_TextChanged(sender As Object, e As EventArgs) Handles j4.TextChanged
        j4.Text = formulaire4j.infoj4.Text + " :"
    End Sub

    Private Sub jeu2j_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        '  Dim pic As PictureBox
        '  Dim echanger As Button
        '   echanger = Me.Controls("cmdEchanger")
        '   echanger.Enabled = False
        '  pic = echanger.Controls("cmdEchanger")
        '   pic.AllowDrop = True
        For i = 1 To 6
            AddHandler grpPions.Controls("pic" & i.ToString).MouseMove, AddressOf pic_MouseMove
        Next
    End Sub
    Private Sub pic_MouseMove(sender As Object, e As MouseEventArgs) Handles pic1.MouseMove
        Dim pic As PictureBox = sender
        Dim rep As DragDropEffects
        If e.Button = MouseButtons.Left Then
            rep = pic.DoDragDrop(pic.Image, DragDropEffects.Move)
        End If
    End Sub

    Private Sub cmdEchanger_DragEnter(sender As Object, e As DragEventArgs) Handles cmdEchanger.DragEnter
        If e.Data.GetDataPresent(DataFormats.Bitmap) Then
            e.Effect = DragDropEffects.Move
        Else
            e.Effect = DragDropEffects.None
        End If
    End Sub

    Private Sub cmdEchanger_DragDrop(sender As Object, e As DragEventArgs) Handles cmdEchanger.DragDrop
        Dim pic As PictureBox = sender
        pic.Image = e.Data.GetData(pic.Image.GetType.ToString)
    End Sub
End Class
