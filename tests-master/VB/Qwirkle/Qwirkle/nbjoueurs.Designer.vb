﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class nbjoueurs
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.quitter = New System.Windows.Forms.Button()
        Me.cmdMenu = New System.Windows.Forms.Button()
        Me.nbjoueur = New System.Windows.Forms.Label()
        Me.twop = New System.Windows.Forms.Button()
        Me.threep = New System.Windows.Forms.Button()
        Me.fourp = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Comic Sans MS", 48.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.LightSkyBlue
        Me.Label1.Location = New System.Drawing.Point(257, 21)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(276, 90)
        Me.Label1.TabIndex = 14
        Me.Label1.Text = "Qwirkle"
        '
        'quitter
        '
        Me.quitter.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.quitter.Font = New System.Drawing.Font("Franklin Gothic Medium", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.quitter.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.quitter.Location = New System.Drawing.Point(743, 12)
        Me.quitter.Name = "quitter"
        Me.quitter.Size = New System.Drawing.Size(45, 45)
        Me.quitter.TabIndex = 15
        Me.quitter.Text = "X"
        Me.quitter.UseVisualStyleBackColor = False
        '
        'cmdMenu
        '
        Me.cmdMenu.BackColor = System.Drawing.Color.DodgerBlue
        Me.cmdMenu.Font = New System.Drawing.Font("Comic Sans MS", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdMenu.ForeColor = System.Drawing.SystemColors.HighlightText
        Me.cmdMenu.Location = New System.Drawing.Point(12, 17)
        Me.cmdMenu.Name = "cmdMenu"
        Me.cmdMenu.Size = New System.Drawing.Size(77, 33)
        Me.cmdMenu.TabIndex = 16
        Me.cmdMenu.Text = "Menu"
        Me.cmdMenu.UseVisualStyleBackColor = False
        '
        'nbjoueur
        '
        Me.nbjoueur.AutoSize = True
        Me.nbjoueur.Font = New System.Drawing.Font("Comic Sans MS", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nbjoueur.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.nbjoueur.Location = New System.Drawing.Point(241, 146)
        Me.nbjoueur.Name = "nbjoueur"
        Me.nbjoueur.Size = New System.Drawing.Size(321, 30)
        Me.nbjoueur.TabIndex = 17
        Me.nbjoueur.Text = "Entrez le nombre de joueurs :"
        '
        'twop
        '
        Me.twop.BackColor = System.Drawing.Color.DodgerBlue
        Me.twop.Font = New System.Drawing.Font("Comic Sans MS", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.twop.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.twop.Location = New System.Drawing.Point(319, 200)
        Me.twop.Name = "twop"
        Me.twop.Size = New System.Drawing.Size(150, 40)
        Me.twop.TabIndex = 18
        Me.twop.Text = "2 joueurs"
        Me.twop.UseVisualStyleBackColor = False
        '
        'threep
        '
        Me.threep.BackColor = System.Drawing.Color.Brown
        Me.threep.Font = New System.Drawing.Font("Comic Sans MS", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.threep.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.threep.Location = New System.Drawing.Point(319, 257)
        Me.threep.Name = "threep"
        Me.threep.Size = New System.Drawing.Size(150, 40)
        Me.threep.TabIndex = 19
        Me.threep.Text = "3 joueurs"
        Me.threep.UseVisualStyleBackColor = False
        '
        'fourp
        '
        Me.fourp.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.fourp.BackColor = System.Drawing.Color.DarkGreen
        Me.fourp.Font = New System.Drawing.Font("Comic Sans MS", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.fourp.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.fourp.Location = New System.Drawing.Point(319, 315)
        Me.fourp.Name = "fourp"
        Me.fourp.Size = New System.Drawing.Size(150, 40)
        Me.fourp.TabIndex = 20
        Me.fourp.Text = "4 joueurs"
        Me.fourp.UseVisualStyleBackColor = False
        '
        'nbjoueurs
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(800, 450)
        Me.Controls.Add(Me.fourp)
        Me.Controls.Add(Me.threep)
        Me.Controls.Add(Me.twop)
        Me.Controls.Add(Me.nbjoueur)
        Me.Controls.Add(Me.cmdMenu)
        Me.Controls.Add(Me.quitter)
        Me.Controls.Add(Me.Label1)
        Me.Name = "nbjoueurs"
        Me.Text = "nbjoueurs"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Label1 As Label
    Friend WithEvents quitter As Button
    Friend WithEvents cmdMenu As Button
    Friend WithEvents nbjoueur As Label
    Friend WithEvents twop As Button
    Friend WithEvents threep As Button
    Friend WithEvents fourp As Button
End Class
