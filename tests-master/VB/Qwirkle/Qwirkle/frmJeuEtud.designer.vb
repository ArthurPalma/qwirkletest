﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmJeuEtud
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmJeuEtud))
        Me.picT14 = New System.Windows.Forms.PictureBox()
        Me.picT13 = New System.Windows.Forms.PictureBox()
        Me.picT12 = New System.Windows.Forms.PictureBox()
        Me.picT11 = New System.Windows.Forms.PictureBox()
        Me.picCol6 = New System.Windows.Forms.PictureBox()
        Me.picCol5 = New System.Windows.Forms.PictureBox()
        Me.picCol4 = New System.Windows.Forms.PictureBox()
        Me.picCol3 = New System.Windows.Forms.PictureBox()
        Me.picCol2 = New System.Windows.Forms.PictureBox()
        Me.picRep14 = New System.Windows.Forms.PictureBox()
        Me.picRep13 = New System.Windows.Forms.PictureBox()
        Me.picRep12 = New System.Windows.Forms.PictureBox()
        Me.picRep11 = New System.Windows.Forms.PictureBox()
        Me.picRep24 = New System.Windows.Forms.PictureBox()
        Me.picT21 = New System.Windows.Forms.PictureBox()
        Me.picRep23 = New System.Windows.Forms.PictureBox()
        Me.picT22 = New System.Windows.Forms.PictureBox()
        Me.picRep22 = New System.Windows.Forms.PictureBox()
        Me.picT23 = New System.Windows.Forms.PictureBox()
        Me.picRep21 = New System.Windows.Forms.PictureBox()
        Me.picT24 = New System.Windows.Forms.PictureBox()
        Me.picRep34 = New System.Windows.Forms.PictureBox()
        Me.picT31 = New System.Windows.Forms.PictureBox()
        Me.picRep33 = New System.Windows.Forms.PictureBox()
        Me.picT32 = New System.Windows.Forms.PictureBox()
        Me.picRep32 = New System.Windows.Forms.PictureBox()
        Me.picT33 = New System.Windows.Forms.PictureBox()
        Me.picRep31 = New System.Windows.Forms.PictureBox()
        Me.picT34 = New System.Windows.Forms.PictureBox()
        Me.picRep44 = New System.Windows.Forms.PictureBox()
        Me.picT41 = New System.Windows.Forms.PictureBox()
        Me.picRep43 = New System.Windows.Forms.PictureBox()
        Me.picT42 = New System.Windows.Forms.PictureBox()
        Me.picRep42 = New System.Windows.Forms.PictureBox()
        Me.picT43 = New System.Windows.Forms.PictureBox()
        Me.picRep41 = New System.Windows.Forms.PictureBox()
        Me.picT44 = New System.Windows.Forms.PictureBox()
        Me.picRep54 = New System.Windows.Forms.PictureBox()
        Me.picT51 = New System.Windows.Forms.PictureBox()
        Me.picRep53 = New System.Windows.Forms.PictureBox()
        Me.picT52 = New System.Windows.Forms.PictureBox()
        Me.picRep52 = New System.Windows.Forms.PictureBox()
        Me.picT53 = New System.Windows.Forms.PictureBox()
        Me.picRep51 = New System.Windows.Forms.PictureBox()
        Me.picT54 = New System.Windows.Forms.PictureBox()
        Me.picRep64 = New System.Windows.Forms.PictureBox()
        Me.picT61 = New System.Windows.Forms.PictureBox()
        Me.picRep63 = New System.Windows.Forms.PictureBox()
        Me.picT62 = New System.Windows.Forms.PictureBox()
        Me.picRep62 = New System.Windows.Forms.PictureBox()
        Me.picT63 = New System.Windows.Forms.PictureBox()
        Me.picRep61 = New System.Windows.Forms.PictureBox()
        Me.picT64 = New System.Windows.Forms.PictureBox()
        Me.picRep74 = New System.Windows.Forms.PictureBox()
        Me.picT71 = New System.Windows.Forms.PictureBox()
        Me.picRep73 = New System.Windows.Forms.PictureBox()
        Me.picT72 = New System.Windows.Forms.PictureBox()
        Me.picRep72 = New System.Windows.Forms.PictureBox()
        Me.picT73 = New System.Windows.Forms.PictureBox()
        Me.picRep71 = New System.Windows.Forms.PictureBox()
        Me.picT74 = New System.Windows.Forms.PictureBox()
        Me.picRep84 = New System.Windows.Forms.PictureBox()
        Me.picT81 = New System.Windows.Forms.PictureBox()
        Me.picRep83 = New System.Windows.Forms.PictureBox()
        Me.picT82 = New System.Windows.Forms.PictureBox()
        Me.picRep82 = New System.Windows.Forms.PictureBox()
        Me.picT83 = New System.Windows.Forms.PictureBox()
        Me.picRep81 = New System.Windows.Forms.PictureBox()
        Me.picT84 = New System.Windows.Forms.PictureBox()
        Me.picRep94 = New System.Windows.Forms.PictureBox()
        Me.picT91 = New System.Windows.Forms.PictureBox()
        Me.picRep93 = New System.Windows.Forms.PictureBox()
        Me.picT92 = New System.Windows.Forms.PictureBox()
        Me.picRep92 = New System.Windows.Forms.PictureBox()
        Me.picT93 = New System.Windows.Forms.PictureBox()
        Me.picRep91 = New System.Windows.Forms.PictureBox()
        Me.picT94 = New System.Windows.Forms.PictureBox()
        Me.picRep104 = New System.Windows.Forms.PictureBox()
        Me.picT101 = New System.Windows.Forms.PictureBox()
        Me.picRep103 = New System.Windows.Forms.PictureBox()
        Me.picT102 = New System.Windows.Forms.PictureBox()
        Me.picRep102 = New System.Windows.Forms.PictureBox()
        Me.picT103 = New System.Windows.Forms.PictureBox()
        Me.picRep101 = New System.Windows.Forms.PictureBox()
        Me.picT104 = New System.Windows.Forms.PictureBox()
        Me.grpT1 = New System.Windows.Forms.GroupBox()
        Me.cmdT1 = New System.Windows.Forms.Button()
        Me.grpT2 = New System.Windows.Forms.GroupBox()
        Me.cmdT2 = New System.Windows.Forms.Button()
        Me.grpT3 = New System.Windows.Forms.GroupBox()
        Me.cmdT3 = New System.Windows.Forms.Button()
        Me.grpPions = New System.Windows.Forms.GroupBox()
        Me.picColR = New System.Windows.Forms.PictureBox()
        Me.grpT4 = New System.Windows.Forms.GroupBox()
        Me.cmdT4 = New System.Windows.Forms.Button()
        Me.grpT5 = New System.Windows.Forms.GroupBox()
        Me.cmdT5 = New System.Windows.Forms.Button()
        Me.grpT6 = New System.Windows.Forms.GroupBox()
        Me.cmdT6 = New System.Windows.Forms.Button()
        Me.grpT7 = New System.Windows.Forms.GroupBox()
        Me.cmdT7 = New System.Windows.Forms.Button()
        Me.grpT8 = New System.Windows.Forms.GroupBox()
        Me.cmdT8 = New System.Windows.Forms.Button()
        Me.grpT9 = New System.Windows.Forms.GroupBox()
        Me.cmdT9 = New System.Windows.Forms.Button()
        Me.grpT10 = New System.Windows.Forms.GroupBox()
        Me.cmdT10 = New System.Windows.Forms.Button()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.TextBox2 = New System.Windows.Forms.TextBox()
        CType(Me.picT14, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picT13, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picT12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picT11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picCol6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picCol5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picCol4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picCol3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picCol2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picRep14, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picRep13, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picRep12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picRep11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picRep24, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picT21, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picRep23, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picT22, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picRep22, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picT23, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picRep21, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picT24, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picRep34, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picT31, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picRep33, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picT32, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picRep32, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picT33, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picRep31, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picT34, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picRep44, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picT41, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picRep43, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picT42, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picRep42, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picT43, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picRep41, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picT44, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picRep54, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picT51, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picRep53, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picT52, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picRep52, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picT53, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picRep51, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picT54, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picRep64, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picT61, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picRep63, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picT62, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picRep62, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picT63, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picRep61, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picT64, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picRep74, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picT71, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picRep73, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picT72, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picRep72, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picT73, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picRep71, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picT74, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picRep84, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picT81, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picRep83, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picT82, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picRep82, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picT83, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picRep81, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picT84, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picRep94, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picT91, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picRep93, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picT92, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picRep92, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picT93, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picRep91, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picT94, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picRep104, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picT101, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picRep103, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picT102, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picRep102, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picT103, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picRep101, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picT104, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grpT1.SuspendLayout()
        Me.grpT2.SuspendLayout()
        Me.grpT3.SuspendLayout()
        Me.grpPions.SuspendLayout()
        CType(Me.picColR, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grpT4.SuspendLayout()
        Me.grpT5.SuspendLayout()
        Me.grpT6.SuspendLayout()
        Me.grpT7.SuspendLayout()
        Me.grpT8.SuspendLayout()
        Me.grpT9.SuspendLayout()
        Me.grpT10.SuspendLayout()
        Me.SuspendLayout()
        '
        'picT14
        '
        Me.picT14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picT14.Location = New System.Drawing.Point(211, 14)
        Me.picT14.Name = "picT14"
        Me.picT14.Size = New System.Drawing.Size(40, 40)
        Me.picT14.TabIndex = 1
        Me.picT14.TabStop = False
        '
        'picT13
        '
        Me.picT13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picT13.Location = New System.Drawing.Point(165, 14)
        Me.picT13.Name = "picT13"
        Me.picT13.Size = New System.Drawing.Size(40, 40)
        Me.picT13.TabIndex = 2
        Me.picT13.TabStop = False
        '
        'picT12
        '
        Me.picT12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picT12.Location = New System.Drawing.Point(119, 14)
        Me.picT12.Name = "picT12"
        Me.picT12.Size = New System.Drawing.Size(40, 40)
        Me.picT12.TabIndex = 3
        Me.picT12.TabStop = False
        '
        'picT11
        '
        Me.picT11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picT11.Location = New System.Drawing.Point(73, 14)
        Me.picT11.Name = "picT11"
        Me.picT11.Size = New System.Drawing.Size(40, 40)
        Me.picT11.TabIndex = 4
        Me.picT11.TabStop = False
        '
        'picCol6
        '
        Me.picCol6.BackColor = System.Drawing.Color.Black
        Me.picCol6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picCol6.Location = New System.Drawing.Point(240, 14)
        Me.picCol6.Name = "picCol6"
        Me.picCol6.Size = New System.Drawing.Size(40, 40)
        Me.picCol6.TabIndex = 6
        Me.picCol6.TabStop = False
        '
        'picCol5
        '
        Me.picCol5.BackColor = System.Drawing.Color.White
        Me.picCol5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picCol5.Location = New System.Drawing.Point(194, 14)
        Me.picCol5.Name = "picCol5"
        Me.picCol5.Size = New System.Drawing.Size(40, 40)
        Me.picCol5.TabIndex = 7
        Me.picCol5.TabStop = False
        '
        'picCol4
        '
        Me.picCol4.BackColor = System.Drawing.Color.Yellow
        Me.picCol4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picCol4.Location = New System.Drawing.Point(148, 14)
        Me.picCol4.Name = "picCol4"
        Me.picCol4.Size = New System.Drawing.Size(40, 40)
        Me.picCol4.TabIndex = 8
        Me.picCol4.TabStop = False
        '
        'picCol3
        '
        Me.picCol3.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.picCol3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picCol3.Location = New System.Drawing.Point(102, 14)
        Me.picCol3.Name = "picCol3"
        Me.picCol3.Size = New System.Drawing.Size(40, 40)
        Me.picCol3.TabIndex = 9
        Me.picCol3.TabStop = False
        '
        'picCol2
        '
        Me.picCol2.BackColor = System.Drawing.Color.Blue
        Me.picCol2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picCol2.Location = New System.Drawing.Point(56, 14)
        Me.picCol2.Name = "picCol2"
        Me.picCol2.Size = New System.Drawing.Size(40, 40)
        Me.picCol2.TabIndex = 10
        Me.picCol2.TabStop = False
        '
        'picRep14
        '
        Me.picRep14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picRep14.Location = New System.Drawing.Point(32, 38)
        Me.picRep14.Name = "picRep14"
        Me.picRep14.Size = New System.Drawing.Size(16, 16)
        Me.picRep14.TabIndex = 15
        Me.picRep14.TabStop = False
        '
        'picRep13
        '
        Me.picRep13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picRep13.Location = New System.Drawing.Point(8, 38)
        Me.picRep13.Name = "picRep13"
        Me.picRep13.Size = New System.Drawing.Size(16, 16)
        Me.picRep13.TabIndex = 14
        Me.picRep13.TabStop = False
        '
        'picRep12
        '
        Me.picRep12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picRep12.Location = New System.Drawing.Point(32, 14)
        Me.picRep12.Name = "picRep12"
        Me.picRep12.Size = New System.Drawing.Size(16, 16)
        Me.picRep12.TabIndex = 13
        Me.picRep12.TabStop = False
        '
        'picRep11
        '
        Me.picRep11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picRep11.Location = New System.Drawing.Point(8, 14)
        Me.picRep11.Name = "picRep11"
        Me.picRep11.Size = New System.Drawing.Size(16, 16)
        Me.picRep11.TabIndex = 12
        Me.picRep11.TabStop = False
        '
        'picRep24
        '
        Me.picRep24.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picRep24.Location = New System.Drawing.Point(32, 38)
        Me.picRep24.Name = "picRep24"
        Me.picRep24.Size = New System.Drawing.Size(16, 16)
        Me.picRep24.TabIndex = 15
        Me.picRep24.TabStop = False
        '
        'picT21
        '
        Me.picT21.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picT21.Location = New System.Drawing.Point(73, 14)
        Me.picT21.Name = "picT21"
        Me.picT21.Size = New System.Drawing.Size(40, 40)
        Me.picT21.TabIndex = 4
        Me.picT21.TabStop = False
        '
        'picRep23
        '
        Me.picRep23.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picRep23.Location = New System.Drawing.Point(8, 38)
        Me.picRep23.Name = "picRep23"
        Me.picRep23.Size = New System.Drawing.Size(16, 16)
        Me.picRep23.TabIndex = 14
        Me.picRep23.TabStop = False
        '
        'picT22
        '
        Me.picT22.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picT22.Location = New System.Drawing.Point(119, 14)
        Me.picT22.Name = "picT22"
        Me.picT22.Size = New System.Drawing.Size(40, 40)
        Me.picT22.TabIndex = 3
        Me.picT22.TabStop = False
        '
        'picRep22
        '
        Me.picRep22.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picRep22.Location = New System.Drawing.Point(32, 14)
        Me.picRep22.Name = "picRep22"
        Me.picRep22.Size = New System.Drawing.Size(16, 16)
        Me.picRep22.TabIndex = 13
        Me.picRep22.TabStop = False
        '
        'picT23
        '
        Me.picT23.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picT23.Location = New System.Drawing.Point(165, 14)
        Me.picT23.Name = "picT23"
        Me.picT23.Size = New System.Drawing.Size(40, 40)
        Me.picT23.TabIndex = 2
        Me.picT23.TabStop = False
        '
        'picRep21
        '
        Me.picRep21.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picRep21.Location = New System.Drawing.Point(8, 14)
        Me.picRep21.Name = "picRep21"
        Me.picRep21.Size = New System.Drawing.Size(16, 16)
        Me.picRep21.TabIndex = 12
        Me.picRep21.TabStop = False
        '
        'picT24
        '
        Me.picT24.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picT24.Location = New System.Drawing.Point(211, 14)
        Me.picT24.Name = "picT24"
        Me.picT24.Size = New System.Drawing.Size(40, 40)
        Me.picT24.TabIndex = 1
        Me.picT24.TabStop = False
        '
        'picRep34
        '
        Me.picRep34.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picRep34.Location = New System.Drawing.Point(32, 38)
        Me.picRep34.Name = "picRep34"
        Me.picRep34.Size = New System.Drawing.Size(16, 16)
        Me.picRep34.TabIndex = 15
        Me.picRep34.TabStop = False
        '
        'picT31
        '
        Me.picT31.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picT31.Location = New System.Drawing.Point(73, 14)
        Me.picT31.Name = "picT31"
        Me.picT31.Size = New System.Drawing.Size(40, 40)
        Me.picT31.TabIndex = 4
        Me.picT31.TabStop = False
        '
        'picRep33
        '
        Me.picRep33.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picRep33.Location = New System.Drawing.Point(8, 38)
        Me.picRep33.Name = "picRep33"
        Me.picRep33.Size = New System.Drawing.Size(16, 16)
        Me.picRep33.TabIndex = 14
        Me.picRep33.TabStop = False
        '
        'picT32
        '
        Me.picT32.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picT32.Location = New System.Drawing.Point(119, 14)
        Me.picT32.Name = "picT32"
        Me.picT32.Size = New System.Drawing.Size(40, 40)
        Me.picT32.TabIndex = 3
        Me.picT32.TabStop = False
        '
        'picRep32
        '
        Me.picRep32.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picRep32.Location = New System.Drawing.Point(32, 14)
        Me.picRep32.Name = "picRep32"
        Me.picRep32.Size = New System.Drawing.Size(16, 16)
        Me.picRep32.TabIndex = 13
        Me.picRep32.TabStop = False
        '
        'picT33
        '
        Me.picT33.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picT33.Location = New System.Drawing.Point(165, 14)
        Me.picT33.Name = "picT33"
        Me.picT33.Size = New System.Drawing.Size(40, 40)
        Me.picT33.TabIndex = 2
        Me.picT33.TabStop = False
        '
        'picRep31
        '
        Me.picRep31.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picRep31.Location = New System.Drawing.Point(8, 14)
        Me.picRep31.Name = "picRep31"
        Me.picRep31.Size = New System.Drawing.Size(16, 16)
        Me.picRep31.TabIndex = 12
        Me.picRep31.TabStop = False
        '
        'picT34
        '
        Me.picT34.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picT34.Location = New System.Drawing.Point(211, 14)
        Me.picT34.Name = "picT34"
        Me.picT34.Size = New System.Drawing.Size(40, 40)
        Me.picT34.TabIndex = 1
        Me.picT34.TabStop = False
        '
        'picRep44
        '
        Me.picRep44.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picRep44.Location = New System.Drawing.Point(32, 38)
        Me.picRep44.Name = "picRep44"
        Me.picRep44.Size = New System.Drawing.Size(16, 16)
        Me.picRep44.TabIndex = 15
        Me.picRep44.TabStop = False
        '
        'picT41
        '
        Me.picT41.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picT41.Location = New System.Drawing.Point(73, 14)
        Me.picT41.Name = "picT41"
        Me.picT41.Size = New System.Drawing.Size(40, 40)
        Me.picT41.TabIndex = 4
        Me.picT41.TabStop = False
        '
        'picRep43
        '
        Me.picRep43.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picRep43.Location = New System.Drawing.Point(8, 38)
        Me.picRep43.Name = "picRep43"
        Me.picRep43.Size = New System.Drawing.Size(16, 16)
        Me.picRep43.TabIndex = 14
        Me.picRep43.TabStop = False
        '
        'picT42
        '
        Me.picT42.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picT42.Location = New System.Drawing.Point(119, 14)
        Me.picT42.Name = "picT42"
        Me.picT42.Size = New System.Drawing.Size(40, 40)
        Me.picT42.TabIndex = 3
        Me.picT42.TabStop = False
        '
        'picRep42
        '
        Me.picRep42.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picRep42.Location = New System.Drawing.Point(32, 14)
        Me.picRep42.Name = "picRep42"
        Me.picRep42.Size = New System.Drawing.Size(16, 16)
        Me.picRep42.TabIndex = 13
        Me.picRep42.TabStop = False
        '
        'picT43
        '
        Me.picT43.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picT43.Location = New System.Drawing.Point(165, 14)
        Me.picT43.Name = "picT43"
        Me.picT43.Size = New System.Drawing.Size(40, 40)
        Me.picT43.TabIndex = 2
        Me.picT43.TabStop = False
        '
        'picRep41
        '
        Me.picRep41.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picRep41.Location = New System.Drawing.Point(8, 14)
        Me.picRep41.Name = "picRep41"
        Me.picRep41.Size = New System.Drawing.Size(16, 16)
        Me.picRep41.TabIndex = 12
        Me.picRep41.TabStop = False
        '
        'picT44
        '
        Me.picT44.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picT44.Location = New System.Drawing.Point(211, 14)
        Me.picT44.Name = "picT44"
        Me.picT44.Size = New System.Drawing.Size(40, 40)
        Me.picT44.TabIndex = 1
        Me.picT44.TabStop = False
        '
        'picRep54
        '
        Me.picRep54.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picRep54.Location = New System.Drawing.Point(32, 38)
        Me.picRep54.Name = "picRep54"
        Me.picRep54.Size = New System.Drawing.Size(16, 16)
        Me.picRep54.TabIndex = 15
        Me.picRep54.TabStop = False
        '
        'picT51
        '
        Me.picT51.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picT51.Location = New System.Drawing.Point(73, 14)
        Me.picT51.Name = "picT51"
        Me.picT51.Size = New System.Drawing.Size(40, 40)
        Me.picT51.TabIndex = 4
        Me.picT51.TabStop = False
        '
        'picRep53
        '
        Me.picRep53.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picRep53.Location = New System.Drawing.Point(8, 38)
        Me.picRep53.Name = "picRep53"
        Me.picRep53.Size = New System.Drawing.Size(16, 16)
        Me.picRep53.TabIndex = 14
        Me.picRep53.TabStop = False
        '
        'picT52
        '
        Me.picT52.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picT52.Location = New System.Drawing.Point(119, 14)
        Me.picT52.Name = "picT52"
        Me.picT52.Size = New System.Drawing.Size(40, 40)
        Me.picT52.TabIndex = 3
        Me.picT52.TabStop = False
        '
        'picRep52
        '
        Me.picRep52.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picRep52.Location = New System.Drawing.Point(32, 14)
        Me.picRep52.Name = "picRep52"
        Me.picRep52.Size = New System.Drawing.Size(16, 16)
        Me.picRep52.TabIndex = 13
        Me.picRep52.TabStop = False
        '
        'picT53
        '
        Me.picT53.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picT53.Location = New System.Drawing.Point(165, 14)
        Me.picT53.Name = "picT53"
        Me.picT53.Size = New System.Drawing.Size(40, 40)
        Me.picT53.TabIndex = 2
        Me.picT53.TabStop = False
        '
        'picRep51
        '
        Me.picRep51.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picRep51.Location = New System.Drawing.Point(8, 14)
        Me.picRep51.Name = "picRep51"
        Me.picRep51.Size = New System.Drawing.Size(16, 16)
        Me.picRep51.TabIndex = 12
        Me.picRep51.TabStop = False
        '
        'picT54
        '
        Me.picT54.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picT54.Location = New System.Drawing.Point(211, 14)
        Me.picT54.Name = "picT54"
        Me.picT54.Size = New System.Drawing.Size(40, 40)
        Me.picT54.TabIndex = 1
        Me.picT54.TabStop = False
        '
        'picRep64
        '
        Me.picRep64.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picRep64.Location = New System.Drawing.Point(32, 38)
        Me.picRep64.Name = "picRep64"
        Me.picRep64.Size = New System.Drawing.Size(16, 16)
        Me.picRep64.TabIndex = 15
        Me.picRep64.TabStop = False
        '
        'picT61
        '
        Me.picT61.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picT61.Location = New System.Drawing.Point(73, 14)
        Me.picT61.Name = "picT61"
        Me.picT61.Size = New System.Drawing.Size(40, 40)
        Me.picT61.TabIndex = 4
        Me.picT61.TabStop = False
        '
        'picRep63
        '
        Me.picRep63.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picRep63.Location = New System.Drawing.Point(8, 38)
        Me.picRep63.Name = "picRep63"
        Me.picRep63.Size = New System.Drawing.Size(16, 16)
        Me.picRep63.TabIndex = 14
        Me.picRep63.TabStop = False
        '
        'picT62
        '
        Me.picT62.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picT62.Location = New System.Drawing.Point(119, 14)
        Me.picT62.Name = "picT62"
        Me.picT62.Size = New System.Drawing.Size(40, 40)
        Me.picT62.TabIndex = 3
        Me.picT62.TabStop = False
        '
        'picRep62
        '
        Me.picRep62.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picRep62.Location = New System.Drawing.Point(32, 14)
        Me.picRep62.Name = "picRep62"
        Me.picRep62.Size = New System.Drawing.Size(16, 16)
        Me.picRep62.TabIndex = 13
        Me.picRep62.TabStop = False
        '
        'picT63
        '
        Me.picT63.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picT63.Location = New System.Drawing.Point(165, 14)
        Me.picT63.Name = "picT63"
        Me.picT63.Size = New System.Drawing.Size(40, 40)
        Me.picT63.TabIndex = 2
        Me.picT63.TabStop = False
        '
        'picRep61
        '
        Me.picRep61.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picRep61.Location = New System.Drawing.Point(8, 14)
        Me.picRep61.Name = "picRep61"
        Me.picRep61.Size = New System.Drawing.Size(16, 16)
        Me.picRep61.TabIndex = 12
        Me.picRep61.TabStop = False
        '
        'picT64
        '
        Me.picT64.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picT64.Location = New System.Drawing.Point(211, 14)
        Me.picT64.Name = "picT64"
        Me.picT64.Size = New System.Drawing.Size(40, 40)
        Me.picT64.TabIndex = 1
        Me.picT64.TabStop = False
        '
        'picRep74
        '
        Me.picRep74.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picRep74.Location = New System.Drawing.Point(32, 38)
        Me.picRep74.Name = "picRep74"
        Me.picRep74.Size = New System.Drawing.Size(16, 16)
        Me.picRep74.TabIndex = 15
        Me.picRep74.TabStop = False
        '
        'picT71
        '
        Me.picT71.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picT71.Location = New System.Drawing.Point(73, 14)
        Me.picT71.Name = "picT71"
        Me.picT71.Size = New System.Drawing.Size(40, 40)
        Me.picT71.TabIndex = 4
        Me.picT71.TabStop = False
        '
        'picRep73
        '
        Me.picRep73.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picRep73.Location = New System.Drawing.Point(8, 38)
        Me.picRep73.Name = "picRep73"
        Me.picRep73.Size = New System.Drawing.Size(16, 16)
        Me.picRep73.TabIndex = 14
        Me.picRep73.TabStop = False
        '
        'picT72
        '
        Me.picT72.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picT72.Location = New System.Drawing.Point(119, 14)
        Me.picT72.Name = "picT72"
        Me.picT72.Size = New System.Drawing.Size(40, 40)
        Me.picT72.TabIndex = 3
        Me.picT72.TabStop = False
        '
        'picRep72
        '
        Me.picRep72.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picRep72.Location = New System.Drawing.Point(32, 14)
        Me.picRep72.Name = "picRep72"
        Me.picRep72.Size = New System.Drawing.Size(16, 16)
        Me.picRep72.TabIndex = 13
        Me.picRep72.TabStop = False
        '
        'picT73
        '
        Me.picT73.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picT73.Location = New System.Drawing.Point(165, 14)
        Me.picT73.Name = "picT73"
        Me.picT73.Size = New System.Drawing.Size(40, 40)
        Me.picT73.TabIndex = 2
        Me.picT73.TabStop = False
        '
        'picRep71
        '
        Me.picRep71.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picRep71.Location = New System.Drawing.Point(8, 14)
        Me.picRep71.Name = "picRep71"
        Me.picRep71.Size = New System.Drawing.Size(16, 16)
        Me.picRep71.TabIndex = 12
        Me.picRep71.TabStop = False
        '
        'picT74
        '
        Me.picT74.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picT74.Location = New System.Drawing.Point(211, 14)
        Me.picT74.Name = "picT74"
        Me.picT74.Size = New System.Drawing.Size(40, 40)
        Me.picT74.TabIndex = 1
        Me.picT74.TabStop = False
        '
        'picRep84
        '
        Me.picRep84.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picRep84.Location = New System.Drawing.Point(30, 38)
        Me.picRep84.Name = "picRep84"
        Me.picRep84.Size = New System.Drawing.Size(16, 16)
        Me.picRep84.TabIndex = 15
        Me.picRep84.TabStop = False
        '
        'picT81
        '
        Me.picT81.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picT81.Location = New System.Drawing.Point(73, 14)
        Me.picT81.Name = "picT81"
        Me.picT81.Size = New System.Drawing.Size(40, 40)
        Me.picT81.TabIndex = 4
        Me.picT81.TabStop = False
        '
        'picRep83
        '
        Me.picRep83.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picRep83.Location = New System.Drawing.Point(6, 38)
        Me.picRep83.Name = "picRep83"
        Me.picRep83.Size = New System.Drawing.Size(16, 16)
        Me.picRep83.TabIndex = 14
        Me.picRep83.TabStop = False
        '
        'picT82
        '
        Me.picT82.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picT82.Location = New System.Drawing.Point(119, 14)
        Me.picT82.Name = "picT82"
        Me.picT82.Size = New System.Drawing.Size(40, 40)
        Me.picT82.TabIndex = 3
        Me.picT82.TabStop = False
        '
        'picRep82
        '
        Me.picRep82.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picRep82.Location = New System.Drawing.Point(30, 14)
        Me.picRep82.Name = "picRep82"
        Me.picRep82.Size = New System.Drawing.Size(16, 16)
        Me.picRep82.TabIndex = 13
        Me.picRep82.TabStop = False
        '
        'picT83
        '
        Me.picT83.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picT83.Location = New System.Drawing.Point(165, 14)
        Me.picT83.Name = "picT83"
        Me.picT83.Size = New System.Drawing.Size(40, 40)
        Me.picT83.TabIndex = 2
        Me.picT83.TabStop = False
        '
        'picRep81
        '
        Me.picRep81.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picRep81.Location = New System.Drawing.Point(6, 14)
        Me.picRep81.Name = "picRep81"
        Me.picRep81.Size = New System.Drawing.Size(16, 16)
        Me.picRep81.TabIndex = 12
        Me.picRep81.TabStop = False
        '
        'picT84
        '
        Me.picT84.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picT84.Location = New System.Drawing.Point(211, 14)
        Me.picT84.Name = "picT84"
        Me.picT84.Size = New System.Drawing.Size(40, 40)
        Me.picT84.TabIndex = 1
        Me.picT84.TabStop = False
        '
        'picRep94
        '
        Me.picRep94.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picRep94.Location = New System.Drawing.Point(30, 38)
        Me.picRep94.Name = "picRep94"
        Me.picRep94.Size = New System.Drawing.Size(16, 16)
        Me.picRep94.TabIndex = 15
        Me.picRep94.TabStop = False
        '
        'picT91
        '
        Me.picT91.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picT91.Location = New System.Drawing.Point(73, 14)
        Me.picT91.Name = "picT91"
        Me.picT91.Size = New System.Drawing.Size(40, 40)
        Me.picT91.TabIndex = 4
        Me.picT91.TabStop = False
        '
        'picRep93
        '
        Me.picRep93.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picRep93.Location = New System.Drawing.Point(6, 38)
        Me.picRep93.Name = "picRep93"
        Me.picRep93.Size = New System.Drawing.Size(16, 16)
        Me.picRep93.TabIndex = 14
        Me.picRep93.TabStop = False
        '
        'picT92
        '
        Me.picT92.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picT92.Location = New System.Drawing.Point(119, 14)
        Me.picT92.Name = "picT92"
        Me.picT92.Size = New System.Drawing.Size(40, 40)
        Me.picT92.TabIndex = 3
        Me.picT92.TabStop = False
        '
        'picRep92
        '
        Me.picRep92.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picRep92.Location = New System.Drawing.Point(30, 14)
        Me.picRep92.Name = "picRep92"
        Me.picRep92.Size = New System.Drawing.Size(16, 16)
        Me.picRep92.TabIndex = 13
        Me.picRep92.TabStop = False
        '
        'picT93
        '
        Me.picT93.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picT93.Location = New System.Drawing.Point(165, 14)
        Me.picT93.Name = "picT93"
        Me.picT93.Size = New System.Drawing.Size(40, 40)
        Me.picT93.TabIndex = 2
        Me.picT93.TabStop = False
        '
        'picRep91
        '
        Me.picRep91.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picRep91.Location = New System.Drawing.Point(6, 14)
        Me.picRep91.Name = "picRep91"
        Me.picRep91.Size = New System.Drawing.Size(16, 16)
        Me.picRep91.TabIndex = 12
        Me.picRep91.TabStop = False
        '
        'picT94
        '
        Me.picT94.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picT94.Location = New System.Drawing.Point(211, 14)
        Me.picT94.Name = "picT94"
        Me.picT94.Size = New System.Drawing.Size(40, 40)
        Me.picT94.TabIndex = 1
        Me.picT94.TabStop = False
        '
        'picRep104
        '
        Me.picRep104.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picRep104.Location = New System.Drawing.Point(30, 38)
        Me.picRep104.Name = "picRep104"
        Me.picRep104.Size = New System.Drawing.Size(16, 16)
        Me.picRep104.TabIndex = 15
        Me.picRep104.TabStop = False
        '
        'picT101
        '
        Me.picT101.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picT101.Location = New System.Drawing.Point(73, 14)
        Me.picT101.Name = "picT101"
        Me.picT101.Size = New System.Drawing.Size(40, 40)
        Me.picT101.TabIndex = 4
        Me.picT101.TabStop = False
        '
        'picRep103
        '
        Me.picRep103.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picRep103.Location = New System.Drawing.Point(6, 38)
        Me.picRep103.Name = "picRep103"
        Me.picRep103.Size = New System.Drawing.Size(16, 16)
        Me.picRep103.TabIndex = 14
        Me.picRep103.TabStop = False
        '
        'picT102
        '
        Me.picT102.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picT102.Location = New System.Drawing.Point(119, 14)
        Me.picT102.Name = "picT102"
        Me.picT102.Size = New System.Drawing.Size(40, 40)
        Me.picT102.TabIndex = 3
        Me.picT102.TabStop = False
        '
        'picRep102
        '
        Me.picRep102.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picRep102.Location = New System.Drawing.Point(30, 14)
        Me.picRep102.Name = "picRep102"
        Me.picRep102.Size = New System.Drawing.Size(16, 16)
        Me.picRep102.TabIndex = 13
        Me.picRep102.TabStop = False
        '
        'picT103
        '
        Me.picT103.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picT103.Location = New System.Drawing.Point(165, 14)
        Me.picT103.Name = "picT103"
        Me.picT103.Size = New System.Drawing.Size(40, 40)
        Me.picT103.TabIndex = 2
        Me.picT103.TabStop = False
        '
        'picRep101
        '
        Me.picRep101.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picRep101.Location = New System.Drawing.Point(6, 14)
        Me.picRep101.Name = "picRep101"
        Me.picRep101.Size = New System.Drawing.Size(16, 16)
        Me.picRep101.TabIndex = 12
        Me.picRep101.TabStop = False
        '
        'picT104
        '
        Me.picT104.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picT104.Location = New System.Drawing.Point(211, 14)
        Me.picT104.Name = "picT104"
        Me.picT104.Size = New System.Drawing.Size(40, 40)
        Me.picT104.TabIndex = 1
        Me.picT104.TabStop = False
        '
        'grpT1
        '
        Me.grpT1.Controls.Add(Me.cmdT1)
        Me.grpT1.Controls.Add(Me.picRep14)
        Me.grpT1.Controls.Add(Me.picT11)
        Me.grpT1.Controls.Add(Me.picT14)
        Me.grpT1.Controls.Add(Me.picT12)
        Me.grpT1.Controls.Add(Me.picT13)
        Me.grpT1.Controls.Add(Me.picRep13)
        Me.grpT1.Controls.Add(Me.picRep11)
        Me.grpT1.Controls.Add(Me.picRep12)
        Me.grpT1.Location = New System.Drawing.Point(274, 565)
        Me.grpT1.Name = "grpT1"
        Me.grpT1.Size = New System.Drawing.Size(322, 60)
        Me.grpT1.TabIndex = 18
        Me.grpT1.TabStop = False
        '
        'cmdT1
        '
        Me.cmdT1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.cmdT1.Image = CType(resources.GetObject("cmdT1.Image"), System.Drawing.Image)
        Me.cmdT1.Location = New System.Drawing.Point(274, 14)
        Me.cmdT1.Name = "cmdT1"
        Me.cmdT1.Size = New System.Drawing.Size(40, 40)
        Me.cmdT1.TabIndex = 16
        Me.cmdT1.UseVisualStyleBackColor = True
        '
        'grpT2
        '
        Me.grpT2.Controls.Add(Me.cmdT2)
        Me.grpT2.Controls.Add(Me.picRep24)
        Me.grpT2.Controls.Add(Me.picT21)
        Me.grpT2.Controls.Add(Me.picT24)
        Me.grpT2.Controls.Add(Me.picT22)
        Me.grpT2.Controls.Add(Me.picT23)
        Me.grpT2.Controls.Add(Me.picRep23)
        Me.grpT2.Controls.Add(Me.picRep21)
        Me.grpT2.Controls.Add(Me.picRep22)
        Me.grpT2.Location = New System.Drawing.Point(274, 508)
        Me.grpT2.Name = "grpT2"
        Me.grpT2.Size = New System.Drawing.Size(322, 60)
        Me.grpT2.TabIndex = 19
        Me.grpT2.TabStop = False
        '
        'cmdT2
        '
        Me.cmdT2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.cmdT2.Image = CType(resources.GetObject("cmdT2.Image"), System.Drawing.Image)
        Me.cmdT2.Location = New System.Drawing.Point(274, 14)
        Me.cmdT2.Name = "cmdT2"
        Me.cmdT2.Size = New System.Drawing.Size(40, 40)
        Me.cmdT2.TabIndex = 17
        Me.cmdT2.UseVisualStyleBackColor = True
        '
        'grpT3
        '
        Me.grpT3.Controls.Add(Me.cmdT3)
        Me.grpT3.Controls.Add(Me.picT31)
        Me.grpT3.Controls.Add(Me.picRep34)
        Me.grpT3.Controls.Add(Me.picT34)
        Me.grpT3.Controls.Add(Me.picT32)
        Me.grpT3.Controls.Add(Me.picT33)
        Me.grpT3.Controls.Add(Me.picRep32)
        Me.grpT3.Controls.Add(Me.picRep33)
        Me.grpT3.Controls.Add(Me.picRep31)
        Me.grpT3.Location = New System.Drawing.Point(274, 451)
        Me.grpT3.Name = "grpT3"
        Me.grpT3.Size = New System.Drawing.Size(322, 60)
        Me.grpT3.TabIndex = 20
        Me.grpT3.TabStop = False
        '
        'cmdT3
        '
        Me.cmdT3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.cmdT3.Image = CType(resources.GetObject("cmdT3.Image"), System.Drawing.Image)
        Me.cmdT3.Location = New System.Drawing.Point(274, 14)
        Me.cmdT3.Name = "cmdT3"
        Me.cmdT3.Size = New System.Drawing.Size(40, 40)
        Me.cmdT3.TabIndex = 18
        Me.cmdT3.UseVisualStyleBackColor = True
        '
        'grpPions
        '
        Me.grpPions.Controls.Add(Me.picColR)
        Me.grpPions.Controls.Add(Me.picCol2)
        Me.grpPions.Controls.Add(Me.picCol3)
        Me.grpPions.Controls.Add(Me.picCol4)
        Me.grpPions.Controls.Add(Me.picCol5)
        Me.grpPions.Controls.Add(Me.picCol6)
        Me.grpPions.Location = New System.Drawing.Point(291, 631)
        Me.grpPions.Name = "grpPions"
        Me.grpPions.Size = New System.Drawing.Size(288, 60)
        Me.grpPions.TabIndex = 21
        Me.grpPions.TabStop = False
        '
        'picColR
        '
        Me.picColR.BackColor = System.Drawing.Color.Red
        Me.picColR.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picColR.Location = New System.Drawing.Point(10, 14)
        Me.picColR.Name = "picColR"
        Me.picColR.Size = New System.Drawing.Size(40, 40)
        Me.picColR.TabIndex = 11
        Me.picColR.TabStop = False
        '
        'grpT4
        '
        Me.grpT4.Controls.Add(Me.picT41)
        Me.grpT4.Controls.Add(Me.picT42)
        Me.grpT4.Controls.Add(Me.cmdT4)
        Me.grpT4.Controls.Add(Me.picT43)
        Me.grpT4.Controls.Add(Me.picRep41)
        Me.grpT4.Controls.Add(Me.picT44)
        Me.grpT4.Controls.Add(Me.picRep44)
        Me.grpT4.Controls.Add(Me.picRep42)
        Me.grpT4.Controls.Add(Me.picRep43)
        Me.grpT4.Location = New System.Drawing.Point(274, 394)
        Me.grpT4.Name = "grpT4"
        Me.grpT4.Size = New System.Drawing.Size(322, 60)
        Me.grpT4.TabIndex = 21
        Me.grpT4.TabStop = False
        '
        'cmdT4
        '
        Me.cmdT4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.cmdT4.Image = CType(resources.GetObject("cmdT4.Image"), System.Drawing.Image)
        Me.cmdT4.Location = New System.Drawing.Point(274, 14)
        Me.cmdT4.Name = "cmdT4"
        Me.cmdT4.Size = New System.Drawing.Size(40, 40)
        Me.cmdT4.TabIndex = 18
        Me.cmdT4.UseVisualStyleBackColor = True
        '
        'grpT5
        '
        Me.grpT5.Controls.Add(Me.TextBox2)
        Me.grpT5.Controls.Add(Me.picT51)
        Me.grpT5.Controls.Add(Me.picT52)
        Me.grpT5.Controls.Add(Me.picRep54)
        Me.grpT5.Controls.Add(Me.picT53)
        Me.grpT5.Controls.Add(Me.cmdT5)
        Me.grpT5.Controls.Add(Me.picT54)
        Me.grpT5.Controls.Add(Me.picRep51)
        Me.grpT5.Controls.Add(Me.picRep53)
        Me.grpT5.Controls.Add(Me.picRep52)
        Me.grpT5.Location = New System.Drawing.Point(274, 337)
        Me.grpT5.Name = "grpT5"
        Me.grpT5.Size = New System.Drawing.Size(322, 60)
        Me.grpT5.TabIndex = 22
        Me.grpT5.TabStop = False
        '
        'cmdT5
        '
        Me.cmdT5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.cmdT5.Image = CType(resources.GetObject("cmdT5.Image"), System.Drawing.Image)
        Me.cmdT5.Location = New System.Drawing.Point(274, 14)
        Me.cmdT5.Name = "cmdT5"
        Me.cmdT5.Size = New System.Drawing.Size(40, 40)
        Me.cmdT5.TabIndex = 18
        Me.cmdT5.UseVisualStyleBackColor = True
        '
        'grpT6
        '
        Me.grpT6.Controls.Add(Me.picT61)
        Me.grpT6.Controls.Add(Me.picT62)
        Me.grpT6.Controls.Add(Me.picRep64)
        Me.grpT6.Controls.Add(Me.picT63)
        Me.grpT6.Controls.Add(Me.cmdT6)
        Me.grpT6.Controls.Add(Me.picT64)
        Me.grpT6.Controls.Add(Me.picRep61)
        Me.grpT6.Controls.Add(Me.picRep63)
        Me.grpT6.Controls.Add(Me.picRep62)
        Me.grpT6.Location = New System.Drawing.Point(274, 280)
        Me.grpT6.Name = "grpT6"
        Me.grpT6.Size = New System.Drawing.Size(322, 60)
        Me.grpT6.TabIndex = 23
        Me.grpT6.TabStop = False
        '
        'cmdT6
        '
        Me.cmdT6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.cmdT6.Image = CType(resources.GetObject("cmdT6.Image"), System.Drawing.Image)
        Me.cmdT6.Location = New System.Drawing.Point(274, 14)
        Me.cmdT6.Name = "cmdT6"
        Me.cmdT6.Size = New System.Drawing.Size(40, 40)
        Me.cmdT6.TabIndex = 18
        Me.cmdT6.UseVisualStyleBackColor = True
        '
        'grpT7
        '
        Me.grpT7.Controls.Add(Me.picT71)
        Me.grpT7.Controls.Add(Me.picT72)
        Me.grpT7.Controls.Add(Me.picRep74)
        Me.grpT7.Controls.Add(Me.picT73)
        Me.grpT7.Controls.Add(Me.cmdT7)
        Me.grpT7.Controls.Add(Me.picT74)
        Me.grpT7.Controls.Add(Me.picRep73)
        Me.grpT7.Controls.Add(Me.picRep71)
        Me.grpT7.Controls.Add(Me.picRep72)
        Me.grpT7.Location = New System.Drawing.Point(274, 223)
        Me.grpT7.Name = "grpT7"
        Me.grpT7.Size = New System.Drawing.Size(322, 60)
        Me.grpT7.TabIndex = 24
        Me.grpT7.TabStop = False
        '
        'cmdT7
        '
        Me.cmdT7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.cmdT7.Image = CType(resources.GetObject("cmdT7.Image"), System.Drawing.Image)
        Me.cmdT7.Location = New System.Drawing.Point(274, 14)
        Me.cmdT7.Name = "cmdT7"
        Me.cmdT7.Size = New System.Drawing.Size(40, 40)
        Me.cmdT7.TabIndex = 18
        Me.cmdT7.UseVisualStyleBackColor = True
        '
        'grpT8
        '
        Me.grpT8.Controls.Add(Me.picT81)
        Me.grpT8.Controls.Add(Me.picT82)
        Me.grpT8.Controls.Add(Me.picRep84)
        Me.grpT8.Controls.Add(Me.picT83)
        Me.grpT8.Controls.Add(Me.cmdT8)
        Me.grpT8.Controls.Add(Me.picT84)
        Me.grpT8.Controls.Add(Me.picRep81)
        Me.grpT8.Controls.Add(Me.picRep83)
        Me.grpT8.Controls.Add(Me.picRep82)
        Me.grpT8.Location = New System.Drawing.Point(274, 166)
        Me.grpT8.Name = "grpT8"
        Me.grpT8.Size = New System.Drawing.Size(322, 60)
        Me.grpT8.TabIndex = 25
        Me.grpT8.TabStop = False
        '
        'cmdT8
        '
        Me.cmdT8.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.cmdT8.Image = CType(resources.GetObject("cmdT8.Image"), System.Drawing.Image)
        Me.cmdT8.Location = New System.Drawing.Point(274, 14)
        Me.cmdT8.Name = "cmdT8"
        Me.cmdT8.Size = New System.Drawing.Size(40, 40)
        Me.cmdT8.TabIndex = 18
        Me.cmdT8.UseVisualStyleBackColor = True
        '
        'grpT9
        '
        Me.grpT9.Controls.Add(Me.picT91)
        Me.grpT9.Controls.Add(Me.picT92)
        Me.grpT9.Controls.Add(Me.picRep94)
        Me.grpT9.Controls.Add(Me.picT93)
        Me.grpT9.Controls.Add(Me.cmdT9)
        Me.grpT9.Controls.Add(Me.picT94)
        Me.grpT9.Controls.Add(Me.picRep93)
        Me.grpT9.Controls.Add(Me.picRep91)
        Me.grpT9.Controls.Add(Me.picRep92)
        Me.grpT9.Location = New System.Drawing.Point(274, 109)
        Me.grpT9.Name = "grpT9"
        Me.grpT9.Size = New System.Drawing.Size(322, 60)
        Me.grpT9.TabIndex = 26
        Me.grpT9.TabStop = False
        '
        'cmdT9
        '
        Me.cmdT9.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.cmdT9.Image = CType(resources.GetObject("cmdT9.Image"), System.Drawing.Image)
        Me.cmdT9.Location = New System.Drawing.Point(274, 14)
        Me.cmdT9.Name = "cmdT9"
        Me.cmdT9.Size = New System.Drawing.Size(40, 40)
        Me.cmdT9.TabIndex = 18
        Me.cmdT9.UseVisualStyleBackColor = True
        '
        'grpT10
        '
        Me.grpT10.Controls.Add(Me.picT101)
        Me.grpT10.Controls.Add(Me.picT102)
        Me.grpT10.Controls.Add(Me.picRep104)
        Me.grpT10.Controls.Add(Me.picT103)
        Me.grpT10.Controls.Add(Me.cmdT10)
        Me.grpT10.Controls.Add(Me.picT104)
        Me.grpT10.Controls.Add(Me.picRep103)
        Me.grpT10.Controls.Add(Me.picRep101)
        Me.grpT10.Controls.Add(Me.picRep102)
        Me.grpT10.Location = New System.Drawing.Point(274, 52)
        Me.grpT10.Name = "grpT10"
        Me.grpT10.Size = New System.Drawing.Size(322, 60)
        Me.grpT10.TabIndex = 27
        Me.grpT10.TabStop = False
        '
        'cmdT10
        '
        Me.cmdT10.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.cmdT10.Image = CType(resources.GetObject("cmdT10.Image"), System.Drawing.Image)
        Me.cmdT10.Location = New System.Drawing.Point(274, 14)
        Me.cmdT10.Name = "cmdT10"
        Me.cmdT10.Size = New System.Drawing.Size(40, 40)
        Me.cmdT10.TabIndex = 18
        Me.cmdT10.UseVisualStyleBackColor = True
        '
        'Timer1
        '
        Me.Timer1.Interval = 1000
        '
        'TextBox2
        '
        Me.TextBox2.Location = New System.Drawing.Point(59, 29)
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Size = New System.Drawing.Size(100, 22)
        Me.TextBox2.TabIndex = 19
        '
        'frmJeuEtud
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(691, 703)
        Me.Controls.Add(Me.grpT10)
        Me.Controls.Add(Me.grpT9)
        Me.Controls.Add(Me.grpT8)
        Me.Controls.Add(Me.grpT7)
        Me.Controls.Add(Me.grpT6)
        Me.Controls.Add(Me.grpT5)
        Me.Controls.Add(Me.grpT4)
        Me.Controls.Add(Me.grpPions)
        Me.Controls.Add(Me.grpT3)
        Me.Controls.Add(Me.grpT2)
        Me.Controls.Add(Me.grpT1)
        Me.Name = "frmJeuEtud"
        Me.Text = "frmJeu"
        CType(Me.picT14, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picT13, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picT12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picT11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picCol6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picCol5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picCol4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picCol3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picCol2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picRep14, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picRep13, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picRep12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picRep11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picRep24, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picT21, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picRep23, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picT22, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picRep22, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picT23, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picRep21, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picT24, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picRep34, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picT31, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picRep33, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picT32, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picRep32, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picT33, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picRep31, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picT34, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picRep44, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picT41, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picRep43, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picT42, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picRep42, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picT43, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picRep41, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picT44, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picRep54, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picT51, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picRep53, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picT52, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picRep52, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picT53, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picRep51, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picT54, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picRep64, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picT61, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picRep63, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picT62, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picRep62, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picT63, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picRep61, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picT64, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picRep74, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picT71, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picRep73, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picT72, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picRep72, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picT73, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picRep71, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picT74, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picRep84, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picT81, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picRep83, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picT82, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picRep82, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picT83, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picRep81, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picT84, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picRep94, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picT91, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picRep93, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picT92, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picRep92, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picT93, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picRep91, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picT94, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picRep104, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picT101, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picRep103, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picT102, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picRep102, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picT103, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picRep101, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picT104, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpT1.ResumeLayout(False)
        Me.grpT2.ResumeLayout(False)
        Me.grpT3.ResumeLayout(False)
        Me.grpPions.ResumeLayout(False)
        CType(Me.picColR, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpT4.ResumeLayout(False)
        Me.grpT5.ResumeLayout(False)
        Me.grpT5.PerformLayout()
        Me.grpT6.ResumeLayout(False)
        Me.grpT7.ResumeLayout(False)
        Me.grpT8.ResumeLayout(False)
        Me.grpT9.ResumeLayout(False)
        Me.grpT10.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub


    Friend WithEvents picT14 As PictureBox
    Friend WithEvents picT13 As PictureBox
    Friend WithEvents picT12 As PictureBox
    Friend WithEvents picT11 As PictureBox
    Friend WithEvents picCol6 As PictureBox
    Friend WithEvents picCol5 As PictureBox
    Friend WithEvents picCol4 As PictureBox
    Friend WithEvents picCol3 As PictureBox
    Friend WithEvents picCol2 As PictureBox
    Friend WithEvents picRep11 As PictureBox
    Friend WithEvents picRep14 As PictureBox
    Friend WithEvents picRep13 As PictureBox
    Friend WithEvents picRep12 As PictureBox
    Friend WithEvents picRep24 As PictureBox
    Friend WithEvents picT21 As PictureBox
    Friend WithEvents picRep23 As PictureBox
    Friend WithEvents picT22 As PictureBox
    Friend WithEvents picRep22 As PictureBox
    Friend WithEvents picT23 As PictureBox
    Friend WithEvents picRep21 As PictureBox
    Friend WithEvents picT24 As PictureBox
    Friend WithEvents picRep34 As PictureBox
    Friend WithEvents picT31 As PictureBox
    Friend WithEvents picRep33 As PictureBox
    Friend WithEvents picT32 As PictureBox
    Friend WithEvents picRep32 As PictureBox
    Friend WithEvents picT33 As PictureBox
    Friend WithEvents picRep31 As PictureBox
    Friend WithEvents picT34 As PictureBox
    Friend WithEvents picRep44 As PictureBox
    Friend WithEvents picT41 As PictureBox
    Friend WithEvents picRep43 As PictureBox
    Friend WithEvents picT42 As PictureBox
    Friend WithEvents picRep42 As PictureBox
    Friend WithEvents picT43 As PictureBox
    Friend WithEvents picRep41 As PictureBox
    Friend WithEvents picT44 As PictureBox
    Friend WithEvents picRep54 As PictureBox
    Friend WithEvents picT51 As PictureBox
    Friend WithEvents picRep53 As PictureBox
    Friend WithEvents picT52 As PictureBox
    Friend WithEvents picRep52 As PictureBox
    Friend WithEvents picT53 As PictureBox
    Friend WithEvents picRep51 As PictureBox
    Friend WithEvents picT54 As PictureBox
    Friend WithEvents picRep64 As PictureBox
    Friend WithEvents picT61 As PictureBox
    Friend WithEvents picRep63 As PictureBox
    Friend WithEvents picT62 As PictureBox
    Friend WithEvents picRep62 As PictureBox
    Friend WithEvents picT63 As PictureBox
    Friend WithEvents picRep61 As PictureBox
    Friend WithEvents picT64 As PictureBox
    Friend WithEvents picRep74 As PictureBox
    Friend WithEvents picT71 As PictureBox
    Friend WithEvents picRep73 As PictureBox
    Friend WithEvents picT72 As PictureBox
    Friend WithEvents picRep72 As PictureBox
    Friend WithEvents picT73 As PictureBox
    Friend WithEvents picRep71 As PictureBox
    Friend WithEvents picT74 As PictureBox
    Friend WithEvents picRep84 As PictureBox
    Friend WithEvents picT81 As PictureBox
    Friend WithEvents picRep83 As PictureBox
    Friend WithEvents picT82 As PictureBox
    Friend WithEvents picRep82 As PictureBox
    Friend WithEvents picT83 As PictureBox
    Friend WithEvents picRep81 As PictureBox
    Friend WithEvents picT84 As PictureBox
    Friend WithEvents picRep94 As PictureBox
    Friend WithEvents picT91 As PictureBox
    Friend WithEvents picRep93 As PictureBox
    Friend WithEvents picT92 As PictureBox
    Friend WithEvents picRep92 As PictureBox
    Friend WithEvents picT93 As PictureBox
    Friend WithEvents picRep91 As PictureBox
    Friend WithEvents picT94 As PictureBox
    Friend WithEvents picRep104 As PictureBox
    Friend WithEvents picT101 As PictureBox
    Friend WithEvents picRep103 As PictureBox
    Friend WithEvents picT102 As PictureBox
    Friend WithEvents picRep102 As PictureBox
    Friend WithEvents picT103 As PictureBox
    Friend WithEvents picRep101 As PictureBox
    Friend WithEvents picT104 As PictureBox
    Friend WithEvents grpT1 As GroupBox
    Friend WithEvents grpT2 As GroupBox
    Friend WithEvents grpT3 As GroupBox
    Friend WithEvents cmdT1 As Button
    Friend WithEvents cmdT2 As Button
    Friend WithEvents cmdT3 As Button
    Friend WithEvents grpPions As GroupBox
    Friend WithEvents grpT4 As GroupBox
    Friend WithEvents cmdT4 As Button
    Friend WithEvents grpT5 As GroupBox
    Friend WithEvents cmdT5 As Button
    Friend WithEvents grpT6 As GroupBox
    Friend WithEvents cmdT6 As Button
    Friend WithEvents grpT7 As GroupBox
    Friend WithEvents cmdT7 As Button
    Friend WithEvents grpT8 As GroupBox
    Friend WithEvents cmdT8 As Button
    Friend WithEvents grpT9 As GroupBox
    Friend WithEvents cmdT9 As Button
    Friend WithEvents grpT10 As GroupBox
    Friend WithEvents cmdT10 As Button
    Friend WithEvents picColR As PictureBox
    Friend WithEvents Timer1 As Timer
    Friend WithEvents TextBox2 As TextBox
End Class
