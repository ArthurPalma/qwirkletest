﻿Public Class formulaire2j
    Private Sub quitter_Click(sender As Object, e As EventArgs) Handles quitter.Click
        Dim Rep As DialogResult
        Rep = MessageBox.Show("Voulez-vous vraiment quitter le jeu?", "Message", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
        If Rep = DialogResult.Yes Then
            Me.Close()
        End If
    End Sub

    Private Sub cmdMenu_Click(sender As Object, e As EventArgs) Handles cmdMenu.Click
        Me.Hide()
        frPagemenu.Show()
    End Sub

    Private Sub retour_Click(sender As Object, e As EventArgs) Handles retour.Click
        Me.Hide()
        nbjoueurs.Show()
    End Sub

    Private Sub valider_Click(sender As Object, e As EventArgs) Handles Valider.Click
        If (infoj1.Text IsNot "" And infoj2.Text IsNot "") Then
            Me.Hide()
            jeu2j.Show()
        End If

    End Sub
End Class