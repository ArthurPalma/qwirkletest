﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class formulaire2j
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cmdMenu = New System.Windows.Forms.Button()
        Me.quitter = New System.Windows.Forms.Button()
        Me.retour = New System.Windows.Forms.Button()
        Me.ID2 = New System.Windows.Forms.Label()
        Me.ID1 = New System.Windows.Forms.Label()
        Me.infojoueur = New System.Windows.Forms.Label()
        Me.infoj2 = New System.Windows.Forms.TextBox()
        Me.infoj1 = New System.Windows.Forms.TextBox()
        Me.Valider = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Comic Sans MS", 48.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.LightSkyBlue
        Me.Label1.Location = New System.Drawing.Point(254, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(276, 90)
        Me.Label1.TabIndex = 14
        Me.Label1.Text = "Qwirkle"
        '
        'cmdMenu
        '
        Me.cmdMenu.BackColor = System.Drawing.Color.DodgerBlue
        Me.cmdMenu.Font = New System.Drawing.Font("Comic Sans MS", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdMenu.ForeColor = System.Drawing.SystemColors.HighlightText
        Me.cmdMenu.Location = New System.Drawing.Point(21, 22)
        Me.cmdMenu.Name = "cmdMenu"
        Me.cmdMenu.Size = New System.Drawing.Size(77, 33)
        Me.cmdMenu.TabIndex = 17
        Me.cmdMenu.Text = "Menu"
        Me.cmdMenu.UseVisualStyleBackColor = False
        '
        'quitter
        '
        Me.quitter.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.quitter.Font = New System.Drawing.Font("Franklin Gothic Medium", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.quitter.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.quitter.Location = New System.Drawing.Point(723, 22)
        Me.quitter.Name = "quitter"
        Me.quitter.Size = New System.Drawing.Size(45, 45)
        Me.quitter.TabIndex = 18
        Me.quitter.Text = "X"
        Me.quitter.UseVisualStyleBackColor = False
        '
        'retour
        '
        Me.retour.BackColor = System.Drawing.Color.Brown
        Me.retour.Font = New System.Drawing.Font("Comic Sans MS", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.retour.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.retour.Location = New System.Drawing.Point(21, 61)
        Me.retour.Name = "retour"
        Me.retour.Size = New System.Drawing.Size(77, 40)
        Me.retour.TabIndex = 20
        Me.retour.Text = "Retour"
        Me.retour.UseVisualStyleBackColor = False
        '
        'ID2
        '
        Me.ID2.AutoSize = True
        Me.ID2.Font = New System.Drawing.Font("Comic Sans MS", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ID2.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.ID2.Location = New System.Drawing.Point(222, 278)
        Me.ID2.Name = "ID2"
        Me.ID2.Size = New System.Drawing.Size(133, 30)
        Me.ID2.TabIndex = 29
        Me.ID2.Text = "ID joueur 2"
        '
        'ID1
        '
        Me.ID1.AutoSize = True
        Me.ID1.Font = New System.Drawing.Font("Comic Sans MS", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ID1.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.ID1.Location = New System.Drawing.Point(222, 222)
        Me.ID1.Name = "ID1"
        Me.ID1.Size = New System.Drawing.Size(133, 30)
        Me.ID1.TabIndex = 28
        Me.ID1.Text = "ID joueur 1"
        '
        'infojoueur
        '
        Me.infojoueur.AutoSize = True
        Me.infojoueur.Font = New System.Drawing.Font("Comic Sans MS", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.infojoueur.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.infojoueur.Location = New System.Drawing.Point(65, 148)
        Me.infojoueur.Name = "infojoueur"
        Me.infojoueur.Size = New System.Drawing.Size(428, 30)
        Me.infojoueur.TabIndex = 27
        Me.infojoueur.Text = "Entrez les informations sur les joueurs :"
        '
        'infoj2
        '
        Me.infoj2.Location = New System.Drawing.Point(405, 283)
        Me.infoj2.Name = "infoj2"
        Me.infoj2.Size = New System.Drawing.Size(188, 20)
        Me.infoj2.TabIndex = 36
        '
        'infoj1
        '
        Me.infoj1.Location = New System.Drawing.Point(406, 227)
        Me.infoj1.Name = "infoj1"
        Me.infoj1.Size = New System.Drawing.Size(188, 20)
        Me.infoj1.TabIndex = 35
        '
        'Valider
        '
        Me.Valider.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Valider.BackColor = System.Drawing.Color.DarkGreen
        Me.Valider.Font = New System.Drawing.Font("Comic Sans MS", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Valider.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Valider.Location = New System.Drawing.Point(325, 339)
        Me.Valider.Name = "Valider"
        Me.Valider.Size = New System.Drawing.Size(150, 40)
        Me.Valider.TabIndex = 38
        Me.Valider.Text = "Valider"
        Me.Valider.UseVisualStyleBackColor = False
        '
        'formulaire2j
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(800, 450)
        Me.Controls.Add(Me.Valider)
        Me.Controls.Add(Me.infoj2)
        Me.Controls.Add(Me.infoj1)
        Me.Controls.Add(Me.ID2)
        Me.Controls.Add(Me.ID1)
        Me.Controls.Add(Me.infojoueur)
        Me.Controls.Add(Me.retour)
        Me.Controls.Add(Me.quitter)
        Me.Controls.Add(Me.cmdMenu)
        Me.Controls.Add(Me.Label1)
        Me.Name = "formulaire2j"
        Me.Text = "formulaire"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Label1 As Label
    Friend WithEvents cmdMenu As Button
    Friend WithEvents quitter As Button
    Friend WithEvents retour As Button
    Friend WithEvents ID2 As Label
    Friend WithEvents ID1 As Label
    Friend WithEvents infojoueur As Label
    Friend WithEvents infoj2 As TextBox
    Friend WithEvents infoj1 As TextBox
    Friend WithEvents Valider As Button
End Class
