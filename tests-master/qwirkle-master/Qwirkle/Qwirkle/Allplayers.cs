﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Qwirkle
{
    public class AllPlayers
    {
        List<Player> playerslist;
        public AllPlayers()
        {
            this.playerslist = new List<Player>();
        }
        public void Addplayers(Player player)
        {
            this.playerslist.Add(player);
        }
        public List<Player> getplayers()
        {
            return this.playerslist;
        }
        public void Getinfos(Pioche pioche)
        {
            Console.WriteLine("entrez le nombres de joueurs");     
            string nbjoueurstring = Console.ReadLine(); int nbjoueurs = 0;
            int.TryParse(nbjoueurstring, out nbjoueurs);
            AllPlayers listp = new AllPlayers();
            foreach (Player P in playerslist)
            {
                listp.Addplayers(P);              
                Console.WriteLine("entrez le pseudo du joueur");
                P.playerID = Console.ReadLine();
                P.playerPoints = 0;                
            }
            Player.inithand(pioche, listp);
        }
        public void InitMax()
        {
            foreach (Player P in playerslist)
            {
                foreach (Piece i in P.hand)
                {
                    foreach (Piece j in P.hand)
                    {
                        int maxform = 0, maxcolor = 0;
                        if (i.CompatibilityColor(j))
                        {
                            maxcolor++;
                        }
                        if (i.CompatibilityForm(j))
                        {
                            maxform++;
                        }
                        if   (maxform<maxcolor)  
                        {
                        if (maxcolor > P.playerturn)
                            {
                                P.playerturn = maxcolor;
                            }
                                
                        }
                        else
                        {
                            if (maxform > P.playerturn)
                            {
                                P.playerturn = maxform;
                            }
                            
                        }
                    }
                }               
            }
        }
        public string initturn(AllPlayers playerslist)
        {
            Pioche listp = new Pioche();
            listp.Piocheinit();
            int max=0;
            string namefirst= "oui";
            playerslist.InitMax();
            foreach (var players in playerslist.getplayers())
            {
                if (players.getorder()> max)
                {
                    max = players.getorder();
                    namefirst= players.getname();
                    
                }
            }
            return namefirst;
        }

        public string setscore()
        {
            int score;
            score = 0;
            string winner= null;
            StringBuilder introduce_builder = new StringBuilder();
            foreach (Player P in playerslist)
            {
                    score = P.playerPoints;
                    winner = P.playerID;
                    introduce_builder.AppendFormat("{0}:{1}\n", winner, score);
            }
            return introduce_builder.ToString().Trim();
        }
        public string getWinner()
        {
            int MaxValue;
            MaxValue = 0;
            string winner = null;
            StringBuilder introduce_builder = new StringBuilder();
            foreach (Player P in playerslist)
            {
                if (P.playerPoints > MaxValue)
                {
                    MaxValue = P.playerPoints;
                    winner = P.playerID;
                }
            }
            introduce_builder.AppendFormat("Le gagnant est : {0}", winner);
            return introduce_builder.ToString().Trim();
        }
    }
}