﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qwirkle
{
    public class Player         //Initialiser les joueurs 
    {
        public string playerID;
        public int playerPoints;
        public int playerturn;
        public List<Piece> hand;


        public Player(string playerID, int playerPoints, int playerturn, List<Piece> hand)
        {
            this.playerID = playerID;
            this.playerPoints = playerPoints;
            this.playerturn = playerturn;
            this.hand = hand;

        }
        public List<Piece> gethand()
        {
            return this.hand;
        }

        public static void inithand(Pioche pioche, AllPlayers playerslist )
        {
            pioche.Piocheinit();

            foreach (var players in playerslist.getplayers())
            {
                for (int index1 = 1; index1 <= 6; index1++)
                {
                    var random = new Random();
                    int index = random.Next(pioche.getpieces().Count);
                    players.hand.Add(pioche.getpieces()[index]);
                    pioche.getpieces().Remove(pioche.getpieces()[index]);
                }
            }
        }
        public static void MAJhand(Pioche pioche, AllPlayers playerslist)
        {
            foreach (var players in playerslist.getplayers())
            {
                if (players.hand.Count < 6)
                {
                    while (players.hand.Count < 6)
                    {
                        var random = new Random();
                        int index = random.Next(pioche.getpieces().Count);
                        players.hand.Add(pioche.getpieces()[index]);
                        pioche.getpieces().Remove(pioche.getpieces()[index]);
                    }
                }
            }
        }

        public string getname()
        {
            return this.playerID;
        }
        public int getscore()
        {
            return this.playerPoints;
        }
        public void addpoints(int points)
        {
            playerPoints += points;
        }

        public int getorder()
        {
            return this.playerturn;
        }

        public string Playernb(int nbjoueur)
        {
            int i;
            Console.WriteLine("saisissez le nombre de joueurs");
            for (i = 0; i <= 4; i++)
            {
                playerID = Console.ReadLine();
            }
            return playerID;
        }
    }
}


